package main

import (
	"encoding/binary"
	"fmt"
	"os"
	"strconv"

	"gitea.com/abgr/aerr"
	base32 "github.com/Dancapistan/gobase32"
)

func main() {
	defer aerr.Catch(func(rec interface{}) {
		fmt.Println(rec)
		os.Exit(1)
	})
	if len(os.Args) < 5 {
		panic(aerr.New(`Enter seperated by space IP addr: ip32 127 0 0 1`))
	}
	ip := [4]byte{}
	var e error
	var n uint64
	for i := 0; i < 4; i++ {
		n, e = strconv.ParseUint(os.Args[len(os.Args)-(1+i)], 10, 8)
		aerr.Panicerr(e, nil)
		ip[3-i] = byte(n)
	}
	fmt.Println(base32.Encode(binary.BigEndian.Uint32(ip[:])).String())
}
